package com.heriak.view;

@FunctionalInterface
public interface Printable {
    void print();
}
