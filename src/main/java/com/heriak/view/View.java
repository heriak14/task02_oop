package com.heriak.view;

import com.heriak.controller.Controller;
import com.heriak.controller.ControllerImpl;
import com.heriak.model.PlaneParameter;
import com.heriak.model.plane.Plane;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;
    private Map<String, PlaneParameter> menuParameters;
    private static Scanner input;

    public View() {
        controller = new ControllerImpl();
        input = new Scanner(System.in);

        menu = new LinkedHashMap<>();
        menu.put("1", " SHOW ALL PLANES");
        menu.put("2", " SORT PLANES");
        menu.put("3", " FIND PLANES");
        menu.put("4", " GET AVERAGE DATA");
        menu.put("5", " GET TOTAL DATA");
        menu.put("6", " GET FULL STATISTIC");
        menu.put("Q", " QUIT");

        menuMethods = new LinkedHashMap<>();
        menuMethods.put("1", this::pressButton1);
        menuMethods.put("2", this::pressButton2);
        menuMethods.put("3", this::pressButton3);
        menuMethods.put("4", this::pressButton4);
        menuMethods.put("5", this::pressButton5);
        menuMethods.put("6", this::pressButton6);
        menuMethods.put("Q", this::pressButtonQ);

        menuParameters = new LinkedHashMap<>();
        for (int i = 0; i < PlaneParameter.values().length; i++) {
            menuParameters.put(String.valueOf(i + 1),
                    PlaneParameter.values()[i]);
        }
    }

    private void pressButton1() {
        List<Plane> planes = controller.getPlanes();
        printPlanes(planes);
        System.out.println();
    }

    private void pressButton2() {
        System.out.println("\n\tParameters for sorting");
        System.out.println("1 " + menuParameters.get("1"));
        System.out.println("2 " + menuParameters.get("2"));
        System.out.print("Choose one:");
        int n = input.nextInt();

        controller.sortBy(menuParameters.get(String.valueOf(n)));
        System.out.println("Planes are sorted!");
        System.out.println();
    }

    private void pressButton3() {
        System.out.println("\n\tParameters for searching");
        System.out.println("1 " + menuParameters.get("1"));
        System.out.println("2 " + menuParameters.get("2"));
        System.out.print("Choose one: ");
        int n = input.nextInt();

        System.out.print("\nEnter min and max parameter:");
        int min = input.nextInt();
        int max = input.nextInt();

        printPlanes(controller.findBy(menuParameters.get(String.valueOf(n)),
                min, max));
        System.out.println();
    }

    private void pressButton4() {
        System.out.println("\n\tParameters for calculating average data");
        System.out.println("1 " + menuParameters.get("1"));
        System.out.println("2 " + menuParameters.get("2"));
        System.out.print("Choose one: ");
        int n = input.nextInt();
        PlaneParameter param = menuParameters.get(String.valueOf(n));

        System.out.println("Average " + param + " = "
                + controller.getAverage(param));
        System.out.println();
    }

    private void pressButton5() {
        System.out.println("\n\tParameters for calculating total data");
        System.out.println("3 " + menuParameters.get("3"));
        System.out.println("4 " + menuParameters.get("4"));
        System.out.print("Choose one: ");
        int n = input.nextInt();
        PlaneParameter param = menuParameters.get(String.valueOf(n));

        System.out.println("Total " + param + " = "
                + controller.getTotal(param));
        System.out.println();
    }

    private void pressButton6() {
        System.out.println(controller.getStat());
        System.out.println();
    }

    private void pressButtonQ() {
    }

    private void printMenu() {
        System.out.println("\t-----MENU-----");
        for (String k : menu.keySet()) {
            System.out.print(k);
            System.out.println(menu.get(k));
        }
        System.out.print("Enter your choice: ");
    }

    private void printPlanes(List<Plane> planes) {
        int counter = 1;
        for (Plane p : planes) {
            System.out.println(counter++ + ")");
            System.out.println("\t" + p);
            System.out.println("---------------------------------------");
        }
        System.out.println();
    }

    public void show() {
        String key = "";
        do {
            printMenu();
            key = input.next().toUpperCase();
            try {
                menuMethods.get(key).print();
            } catch (Exception e) {
                System.out.println("Incorrect input! Please, try again!");
            }
        } while (!key.equals("Q"));
    }
}
