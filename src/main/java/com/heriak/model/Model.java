package com.heriak.model;

import com.heriak.model.plane.Plane;

import java.util.List;

public interface Model {

    List<Plane> getPlanes();

    void sortByFuelConsumption();
    void sortByFlightRange();

    List<Plane> findByFuelConsumption(double minConsumption,
                                      double maxConsumption);
    List<Plane> findByFlightRange(double minRange, double maxRange);

    double getAverageFuelConsumption();
    double getAverageFlightRange();

    double getTotalPassengerCapacity();
    double getTotalLoadCapacity();

    String getStat();
}
