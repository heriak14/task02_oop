package com.heriak.model;

import com.heriak.model.plane.*;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Airline {

    private List<Plane> planes;

    public Airline() {
        planes = new LinkedList<>();
        planes.add(new PassengerPlane("Boeing 747", 12, 12000, 437));
        planes.add(new CargoPlane("AH-225", 23, 9500, 600));
        planes.add(new CargoPlane("AH-124", 18, 10300, 400));
        planes.add(new MailPlane("AH-52", 7, 8600, 42));
        planes.add(new PassengerPlane("Boeing 737", 3, 3000, 215));
        planes.add(new PassengerPlane("TY-154", 6, 4000, 180));
        planes.add(new PassengerPlane("Aerobus A320", 2.7, 1000, 95));
    }

    public List<Plane> getPlanes() {
        return planes;
    }

    public void sortByFuelConsumption() {
        Collections.sort(planes, (p1, p2) -> {
            if (p1.getFuelConsumption() > p2.getFuelConsumption()) {
                return 1;
            } else if (p1.getFuelConsumption() < p2.getFuelConsumption()) {
                return -1;
            } else {
                return 0;
            }
        });
    }

    public void sortByFlightRange() {
        Collections.sort(planes, (p1, p2) -> {
            if (p1.getFlightRange() > p2.getFlightRange()) {
                return 1;
            } else if (p1.getFlightRange() < p2.getFlightRange()) {
                return -1;
            } else {
                return 0;
            }
        });
    }

    public List<Plane> findByFuelConsumption(double minConsumption,
                                             double maxConsumption) {
        List<Plane> result = new LinkedList<>();
        for (Plane p : planes) {
            if (p.getFuelConsumption() >= minConsumption
                    && p.getFuelConsumption() <= maxConsumption) {
                result.add(p);
            }
        }
        return result;

    }

    public List<Plane> findByFlightRange(double minRange,
                                         double maxRange) {
        List<Plane> result = new LinkedList<>();
        for (Plane p : planes) {
            if (p.getFlightRange() >= minRange
                    && p.getFlightRange() <= maxRange) {
                result.add(p);
            }
        }
        return result;
    }

    public double getAverageFuelConsumption() {
        double avg = 0;
        for (Plane p : planes) {
            avg += p.getFuelConsumption();
        }
        return avg / planes.size();
    }

    public double getAverageFlightRange() {
        double avg = 0;
        for (Plane p : planes) {
            avg += p.getFlightRange();
        }
        return avg / planes.size();
    }

    public int getTotalPassengerCapacity() {
        int total = 0;
        for (Plane p : planes) {
            if (p instanceof PassengerPlane) {
                total += ((PassengerPlane) p).getPassengerCapacity();
            }
        }
        return total;
    }

    public double getTotalLoadCapacity() {
        double total = 0;
        for (Plane p : planes) {
            if (p instanceof TransportPlane) {
                total += ((TransportPlane) p).getLoadCapacity();
            }
        }
        return total;
    }
}
