package com.heriak.model.plane;

public class MailPlane extends TransportPlane {

    private String name;

    public MailPlane(String name, double fuelConsumption,
                     int flightRange, double loadCapacity) {
        super(fuelConsumption, flightRange, loadCapacity);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void fly() {
        System.out.println("Mail plane: " + name + " with "
                + getLoadCapacity() + " tons of mail is flying");
    }

    @Override
    public String toString() {
        return "Mail plane: " + name
                + "\n\tFuel consumption " + getFuelConsumption()
                + "\n\tFlight range " + getFlightRange()
                + "\n\tLoad capacity " + getLoadCapacity();
    }
}
