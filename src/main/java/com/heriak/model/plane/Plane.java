package com.heriak.model.plane;

public abstract class Plane {

    private double fuelConsumption;
    private int flightRange;

    public Plane(double fuelConsumption, int flightRange) {
        this.fuelConsumption = fuelConsumption;
        this.flightRange = flightRange;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public int getFlightRange() {
        return flightRange;
    }

    public void setFlightRange(int flightRange) {
        this.flightRange = flightRange;
    }

    public abstract void fly();
}
