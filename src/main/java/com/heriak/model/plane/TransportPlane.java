package com.heriak.model.plane;

public abstract class TransportPlane extends Plane {

    private double loadCapacity;

    public TransportPlane(double fuelConsumption, int flightRange,
                          double loadCapacity) {
        super(fuelConsumption, flightRange);
        this.loadCapacity = loadCapacity;
    }

    public double getLoadCapacity() {
        return loadCapacity;
    }

    public void setLoadCapacity(int loadCapacity) {
        this.loadCapacity = loadCapacity;
    }
}
