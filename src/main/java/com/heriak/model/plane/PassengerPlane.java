package com.heriak.model.plane;

public class PassengerPlane extends Plane {

    private String name;
    private int passengerCapacity;

    public PassengerPlane(String name, double fuelConsumption,
                          int flightRange, int passengerCapacity) {
        super(fuelConsumption, flightRange);
        this.name = name;
        this.passengerCapacity = passengerCapacity;
    }

    public String getName() {
        return name;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public void fly() {
        System.out.println("Passenger plane: " + name + " with "
                + passengerCapacity + " passengers is flying!");
    }

    @Override
    public String toString() {
        return "Passenger plane: " + name
                + "\n\tFuel consumption " + getFuelConsumption()
                + "\n\tFlight range " + getFlightRange()
                + "\n\tPassenger capacity " + passengerCapacity;
    }
}
