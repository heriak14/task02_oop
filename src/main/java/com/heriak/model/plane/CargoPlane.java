package com.heriak.model.plane;

public class CargoPlane extends TransportPlane {

    private String name;

    public CargoPlane(String name, double fuelConsumption,
                      int flightRange, double loadCapacity) {
        super(fuelConsumption, flightRange, loadCapacity);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void fly() {
        System.out.println("Cargo plane: " + name + " with "
                + getLoadCapacity() + " tons of cargo is flying");
    }

    @Override
    public String toString() {
        return "Cargo plane: " + name
                + "\n\tFuel consumption " + getFuelConsumption()
                + "\n\tFlight range " + getFlightRange()
                + "\n\tLoad capacity " + getLoadCapacity();
    }
}
