package com.heriak.model;

import com.heriak.model.plane.Plane;

import java.util.List;

public class BusinessLogic implements Model {
    private Airline airline;

    public BusinessLogic() {
        airline = new Airline();
    }

    @Override
    public List<Plane> getPlanes() {
        return airline.getPlanes();
    }

    @Override
    public void sortByFuelConsumption() {
        airline.sortByFuelConsumption();
    }

    @Override
    public void sortByFlightRange() {
        airline.sortByFlightRange();
    }

    @Override
    public List<Plane> findByFuelConsumption(double minConsumption,
                                             double maxConsumption) {
        if (minConsumption <= maxConsumption) {
            return airline.findByFuelConsumption(minConsumption,
                    maxConsumption);
        } else {
            return null;
        }
    }

    @Override
    public List<Plane> findByFlightRange(double minRange, double maxRange) {
        if (minRange <= maxRange) {
            return airline.findByFlightRange(minRange, maxRange);
        } else {
            return null;
        }
    }

    @Override
    public double getAverageFuelConsumption() {
        if (airline.getPlanes().size() > 0) {
            return airline.getAverageFuelConsumption();
        } else {
            return 0;
        }
    }

    @Override
    public double getAverageFlightRange() {
        if (airline.getPlanes().size() > 0) {
            return airline.getAverageFlightRange();
        } else {
            return 0;
        }
    }

    @Override
    public double getTotalPassengerCapacity() {
        return airline.getTotalPassengerCapacity();
    }

    @Override
    public double getTotalLoadCapacity() {
        return airline.getTotalLoadCapacity();
    }

    @Override
    public String getStat() {
        return "Average fuel consumption: " + getAverageFuelConsumption()
                + "\nAverage flight range: " + getAverageFlightRange()
                + "\nTotal passenger capacity: " + getTotalPassengerCapacity()
                + "\nTotal load capacity: " + getTotalLoadCapacity();
    }
}
