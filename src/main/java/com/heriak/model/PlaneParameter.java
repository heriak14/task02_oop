package com.heriak.model;

public enum PlaneParameter {
    FUEL_CONSUMPTION {
        @Override
        public String toString() {
            return "Fuel consumption";
        }
    },
    FLIGHT_RANGE {
        @Override
        public String toString() {
            return "Flight range";
        }
    },
    PASSENGER_CAPACITY {
        @Override
        public String toString() {
            return "Passenger capacity";
        }
    },
    LOAD_CAPACITY {
        @Override
        public String toString() {
            return "Load capacity";
        }
    }
}
