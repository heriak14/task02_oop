package com.heriak.controller;

import com.heriak.model.PlaneParameter;
import com.heriak.model.plane.Plane;

import java.util.List;

public interface Controller {
    List<Plane> getPlanes();
    void sortBy(PlaneParameter parameter);
    List<Plane> findBy(PlaneParameter parameter,
                       int minParameter, int maxParameter);
    double getAverage(PlaneParameter parameter);
    double getTotal(PlaneParameter parameters);
    String getStat();
}
