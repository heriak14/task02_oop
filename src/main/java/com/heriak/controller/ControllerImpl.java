package com.heriak.controller;

import com.heriak.model.BusinessLogic;
import com.heriak.model.Model;
import com.heriak.model.PlaneParameter;
import com.heriak.model.plane.Plane;

import java.util.List;

public class ControllerImpl implements Controller {

    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public List<Plane> getPlanes() {
        return model.getPlanes();
    }

    @Override
    public void sortBy(PlaneParameter parameter) {
        switch (parameter) {
            case FUEL_CONSUMPTION:
                model.sortByFuelConsumption();
                break;
            case FLIGHT_RANGE:
                model.sortByFlightRange();
                break;
            default:
                return;
        }
    }

    @Override
    public List<Plane> findBy(PlaneParameter parameter,
                              int minParameter, int maxParameter) {
        switch (parameter) {
            case FUEL_CONSUMPTION:
                return model.findByFuelConsumption(minParameter, maxParameter);
            case FLIGHT_RANGE:
                return model.findByFlightRange(minParameter, maxParameter);
            default:
                return null;
        }
    }

    @Override
    public double getAverage(PlaneParameter parameter) {
        switch (parameter) {
            case FUEL_CONSUMPTION:
                return model.getAverageFuelConsumption();
            case FLIGHT_RANGE:
                return model.getAverageFlightRange();
            default:
                return 0;
        }
    }

    @Override
    public double getTotal(PlaneParameter parameter) {
        switch (parameter) {
            case PASSENGER_CAPACITY:
                return model.getTotalPassengerCapacity();
            case LOAD_CAPACITY:
                return model.getTotalLoadCapacity();
            default:
                return 0;
        }
    }

    @Override
    public String getStat() {
        return model.getStat();
    }
}
